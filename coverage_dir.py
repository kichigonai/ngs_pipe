#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
It calculates coverage descriptive statistics of BAM files in given directory, according to a genomic intervals reference BED file.
Requires installation of the following packages:
    - pandas
    - pybedtools
    - numpy
    - xlsxwriter
    - xlwt

Bed file should contains 6 columns. See format in example below:

#CHR	START	END	LOCUS	SCORE	STRAND
chr1	955552	955753	AGRN_int_01	AGRN	+
chr1	957580	957842	AGRN_int_02	AGRN	+
chr1	970656	970704	AGRN_int_03	AGRN	+

Where LOCUS column contains exon unique id and SCORE column contains gene id (for grouping).

"""

__author__ = "Javier Diez Perez"
__credits__ = ["Javier Diez Perez"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Javier Diez Perez"
__email__ = "jdiezperezj@gmail.com"
__status__ = "Development"


import os
import sys
from string import upper
import argparse
from pandas import DataFrame
import pybedtools
import numpy as np
import xlsxwriter
from xlwt import Workbook, easyxf, Formula
from operator import itemgetter
import multiprocessing


class BaseCoverage(object):
    """ This class calculate coverage statistics for a given set of bases. """
    def __init__(self,base_coverage):
	self.base_coverage = base_coverage
	self.length_design = len(base_coverage)
	self.mean = "{0:.2f}".format(np.mean(self.base_coverage))
	self.median = "{0:.2f}".format(np.median(self.base_coverage))
	self.coverage_values()
	self.cov_summary()
	
    def coverage_values(self):
	""" Returns the base coverage equal or above a given threshold. """
	self.c50=0
	self.c30=0
	self.c20=0
	self.c10=0
	self.c1=0
	for cov in self.base_coverage:
	    if cov >= 50:
		self.c50 += 1
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 30:
		self.c30 += 1
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 20:
		self.c20 += 1
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 10:
		self.c10 += 1
		self.c1 += 1
	    elif cov >= 1:
		self.c1 += 1
	    else:
		self.c0 += 1
    
    def pc_coverage(self,value):
	""" This function calculates the percentage of the original region(s) coveraged at a given threshold. """
	return "{0:.2f}%".format((value/float(self.length_design))*100)
    
    def cov_summary(self):
	""" Represents the percentages of coverage of the targeted region(s) at different threshold values: 1X, 10X, 20X, 30X and 50X. """
	self.pc1 = self.pc_coverage(self.c1)
	self.pc10 = self.pc_coverage(self.c10)
	self.pc20 = self.pc_coverage(self.c20)
	self.pc30 = self.pc_coverage(self.c30)
	self.pc50 = self.pc_coverage(self.c50)
    
    def stats_summary(self):
	""" Return the summary list to display in the report. """
	return [['LENGTH','MEAN','MEDIAN','1X','10X','20X','30X','50X'], [str(self.length_design),self.mean,self.median,self.pc1,self.pc10,self.pc20,self.pc30,self.pc50],]


class CovStats(object):
    """
    Coverage statistics object deal with DataFrame subsets.
    Perform simple statistics calculations.
    """
    def __init__(self,name,group,thr=30,sample=''):
	self.sample = sample
	self.name = name
        self.group = group
	self.thr = thr
	self.length = str(len(self.group))
	self.values = self.group.to_dict()['cov'].values()
	self.chr = self.group.to_dict()['chr'].values()[0]
	self.pos0 = min(self.group.to_dict()['start'].values()) - 1
	self.stop = max(self.group.to_dict()['stop'].values())

    def get_constiguous(self,data):
	""" Return contiguous intervals of not covered regions. """
	intervals =[]
	interval=[]
	for i in data:
	    if len(interval) == 0:
		interval = [i,]
	    else:
		if max(interval) + 1 == i:
                    interval += [i,]
            	else:
                    intervals += [(min(interval),max(interval)),]
                    interval = [i,]
	if len(interval) > 0:
	    intervals += [(min(interval),max(interval)),]
	return intervals
    
    def get_pos_blcov(self,cov=0):
	""" Represent url in Genome Browser for positions not covered. """
	data = [self.pos0 + i for i,j in zip(self.group.to_dict()['pos'].values(),self.values) if j <= cov]
	if len(data) > 0:
	    intervals = self.get_constiguous(data)
	    sorted(intervals,key=itemgetter(0))
	    return ["http://genome-euro.ucsc.edu/cgi-bin/hgTracks?db=hg19&position={0}%3A{1}-{2}".format(self.chr,v[0],v[1]) for v in intervals]
	else:
	    return ['',]

    def get_n_blcov(self,cov=0):
	""" Positions above coverage threshold. """
	return [self.pos0 + i for i,j in zip(self.group.to_dict()['pos'].values(),self.values) if j <= cov]


    def get_pos_abcov(self,cov=0):
	""" Coverage value above a given value. """
	return [i for i,j in zip(self.group.to_dict()['pos'].values(),self.values) if j >= cov]

    def get_mean(self):
	""" Mean coverage value. """
	return "{0:.2f}".format(np.mean(self.values))

    def get_median(self):
	""" Median coverage values. """
	return "{0:.2f}".format(np.median(self.values))

    def get_std(self):
	""" Standard deviation of coverage values for the interval. """
	return "{0:.2f}".format(np.std(self.values))

    def get_min(self):
	""" Min coverage value in the interval. """
	return min(self.values)
    
    def get_max(self):
	""" Max coverage value in the interval. """
        return max(self.values)

    def get_pcAbvThr(self,thr):
	""" Percentage of a region obove a given threshold. """
        return "{0:.2f}".format((len(self.get_pos_abcov(thr))/float(self.length))*100)

    def get_pc0(self):
	""" Percentage of reagion not covered. """
        return "{0:.2f}".format((len(self.get_n_blcov())/float(self.length))*100)

    def get_stats(self):
	""" Return statistics. """
	results = [self.name,self.chr,self.pos0 + 1,self.stop,self.length,self.get_mean(),self.get_std(),self.get_median(),self.get_min(),self.get_max(), self.get_pcAbvThr(1), self.get_pcAbvThr(10), self.get_pcAbvThr(20),self.get_pcAbvThr(self.thr),self.get_pc0()] + self.get_pos_blcov()
	if self.sample != '':
	    results = [self.sample,] + results
	return results

    def get_header(self):
	""" Represent excel header file. """
	header = ['exon','chr','start','stop','length','mean','std','median','min','max','pc_>{thr}X'.format(thr=1),'pc_>{thr}X'.format(thr=10), 'pc_>{thr}X'.format(thr=20),'pc_>{thr}X'.format(thr=self.thr),'pc0','pos_bl_cov']
	if self.sample != '':
	    header = ['sample',] + header
	return header
    
def pc_coverage(base_coverge,thr=10):
    """ Calculate percentage of coverage."""
    length_design = len(base_coverage)
    base_coverage = [c for c in base_coverage if c >= thr]
    return (sum(base_coverage)/float(length_design)) * 100


def calculate_coverage(sample, bam_file, bed_file, thr=30, only_genes=False):
    """ This function calculates coverage for all intervals using PyBedTools. """
    bed = pybedtools.BedTool(bed_file)
    bamfile = pybedtools.BedTool(bam_file)
    
    cols = ['chr','start','stop','locus','score','strand','pos','cov']
    cov_data = [[i.fields[0], np.int(i.fields[1]), np.int(i.fields[2]), i.fields[3], i.fields[4], i.fields[5], np.int(i.fields[6]), np.int(i.fields[7])] for i in bamfile.coverage(bed, d=True)]
    
    cols_d = ['chr','position','locus','score','strand','pos','cov']
    r_data = [(i[0],i[1]+i[6],i[3],i[4],i[5],i[6],i[7]) for i in cov_data ]
    raw_cov = [tuple(map(upper,cols_d)),] + r_data
    
    summary = BaseCoverage([c[7] for c in cov_data]).stats_summary()

    try:
        df = DataFrame(cov_data,columns = cols)
	if only_genes != True:
	    grouped = df.groupby('locus' ,as_index=False)
	    rows = [CovStats(name, group, sample=sample, thr=thr).get_stats() for name,group in grouped]
	else:
	    rows = [[],]

	g_grouped = df.groupby('score' ,as_index=False)
	g_rows = [CovStats(name, group, sample=sample, thr=thr).get_stats() for name,group in g_grouped]

    except Exception,e:
        print e
        pass

    else:
	header = [map(upper,('sample','locus','chr','start','stop','length','mean','std','median','min','max','pc_1X','pc_10X','pc_20X','pc_{thr}X'.format(thr=thr),'pc_0_cov','pos_0_cov')),]
	print 	sample, dict(zip(summary[0], summary[1]))
	
	summary = [['Summary',],['',],] + summary
	g_rows = [['',],['Gene Coverage'],['',],] + header + g_rows
	e_rows = [['Exon Coverage'],['',],] + header + rows
	return {'summary':summary, 'g_rows':g_rows, 'e_rows':e_rows}


def create_xls_sheet(worksheet,tsv_reader,header=False,summary=False):
    """ This function creates an excel worksheet for coverage_dir format. """
    if summary != False:
	for row, data in enumerate(summary):
	    for i in range(len(data)):
		if row == 0 :
		    worksheet.write(row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;''pattern: pattern solid, fore_color gray25;'))
		elif row == 2:
		    worksheet.write(row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;'))
		else:
		    worksheet.write(row,i,unicode(str(data[i])))
	n = len(summary) + 1
	for row, data in enumerate(tsv_reader):
	    for i in range(len(data)):
		if row == 1 :
		    worksheet.write(n+row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;''pattern: pattern solid;''pattern: fore_color gray25;'))
		elif row == 3:
		    worksheet.write(n+row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;'))
		else:
		    if i >= len(header)-1 and data[i].startswith('http'):
			if i < 256:
			    worksheet.write(n+row,i,Formula('HYPERLINK("{0}";"{1}")'.format(data[i],'UCSC_link')), easyxf('font: underline single'))
		    else:
			worksheet.write(n+row,i,unicode(str(data[i])))
    else:
	for row, data in enumerate(tsv_reader):
	    for i in range(len(data)):
		if row == 0 :
		    worksheet.write(row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;''pattern: pattern solid;''pattern: fore_color gray25;'))
		elif row == 2:
		    worksheet.write(row,i,unicode(str(data[i])),easyxf('font: name Arial, bold on ;''borders: bottom thick;'))
		else:
		    if i >= len(header)-1 and data[i].startswith('http'):
			if i< 256:
			    worksheet.write(row,i,Formula('HYPERLINK("{0}";"{1}")'.format(data[i],'UCSC_link')), easyxf('font: underline single'))
		    else:
			worksheet.write(row,i,unicode(str(data[i])))
    return worksheet


def write_excel(output,name,data,header,only_genes):
    """ Write a list of values (a list of lists) to an excel file. """
    xlsx_file = os.path.join(output,'coverage_'+ name + '.xls')
    workbook = Workbook()
    worksheet1 = workbook.add_sheet(u'Genes')
    #create_xls_sheet(worksheet1,genes, header)
    create_xls_sheet(worksheet1,data['g_rows'], header,data['summary'] )
    if only_genes == False:
	worksheet2 = workbook.add_sheet(u'Exons')
	#create_xls_sheet(worksheet2,exons, header)
	create_xls_sheet(worksheet2,data['e_rows'], header, False)
    worksheet3 = workbook.add_sheet(u'Plots')
    #create_xls_sheet(worksheet3,results, header)
    workbook.save(xlsx_file)


def pcoverage(params):
    """ Parallel wrapper function. """
    data = calculate_coverage(params['samplename'],params['bam_file'],params['bed'],params['thr'],only_genes=params['only_genes'])
    write_excel(args.out_dir, "_".join([params['rname'], params['basename']]), data, params['header'],params['only_genes'])
    return


def main(args):
    header = map(upper,['sample','locus','chr','start','stop','length','mean','std','median','min','max','pc_1X','pc_10X','pc_20X','pc_{thr}X'.format(thr=args.thr),'pc_0_cov','pos_0_cov'])
    params = []
    for f in os.listdir(args.in_dir):
	if f.endswith('.bam'):
	    d = {}
	    d['basename'] = f.split('.')[0]
	    d['bam_file'] = os.path.join(args.in_dir,f)
	    head,tail = os.path.split(f)
	    d['samplename'] = tail.split('.')[0]
	    d['bed'] = args.bed
	    d['thr'] = int(args.thr)
	    d['rname'] = args.rname
	    d['header'] = header
	    d['only_genes'] = args.only_genes
	    params.append(d)
    pool = multiprocessing.Pool(args.n_cores)
    pool.map(pcoverage,params)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = '')
    parser.add_argument('-i','--in_dir', required = True, help = 'Processed BAM directory.')
    parser.add_argument('-o','--out_dir', required = True, help = 'Report output directory.')
    parser.add_argument('-b','--bed', required = True, help = 'Bed file representing targeted regions. Only bed 6 file format is allowed, where 4th column contains the unique ID of the intervals. The fith column contains grouping factor, usually locus name, to group intervals (exons).')
    parser.add_argument('-r','--rname', required = True, help = 'Run name. It will be used as suffix for the excel file.')
    parser.add_argument('-s','--sep', required = False, default='.', help = 'Separator character.')
    parser.add_argument('-n','--n_cores',type=int, required = False, default=4, help = 'Number of cores to parallelize. Default value is 4.')
    parser.add_argument('-t','--thr', required = False, default=30, help = 'Coverage threshold value. Default: 30X.')
    parser.add_argument('--only_genes', dest='only_genes', action='store_true', default=False, help = 'Display only gene info.')
    args = parser.parse_args()
    main(args)