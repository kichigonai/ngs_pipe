#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Exome analysis pipeline.
"""
__author__ = "Javier Diez Perez"
__credits__ = ["Javier Diez Perez"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Javier Diez Perez"
__email__ = "jdiezperezj@gmail.com"
__status__ = "Development"

import sys
import os
import argparse
import json
import subprocess
import datetime
import shutil
from datetime import date
from process_sample_sheet import SampleSheetHiSeq



class ExomePipeline(object):
    def __init__(self,args):
        self.args = args
        self.c_time = date.timetuple(date.today())
        self.ANALYSIS = 'EXOME_HiSeq_TrueSight_{year}_{mon}_{day}_{hour}'.format(year = self.c_time.tm_year, mon = self.c_time.tm_mon, day = self.c_time.tm_mday, hour = self.c_time.tm_hour)
        self.SCHEMA = {'configuration':'CONFIGURATION', 'unaligned':'UNALIGNED', 'aligned':'ALIGNED', 'variants':'VARIANTS', 'cnvs':'CNVS','reports':'REPORTS', 'run_reports':'RUN_REPORTS', 'logs':'LOGS'}
        self.DIRS = {}
        self.chem = args.chemistry
        self.aligner = args.mapper
        self.variant_caller = args.variant_caller
        self.milen = 50
        self.cov_thr = 30
        self.run_name = args.run_name
        self.cnv_mode = args.cnv_mode
        self.probes = args.probes
        self.args.interval_list = args.interval_list
        self.full_bed = self.args.full_bed
        self.analysis_name = self.args.analysis_name

    def write_config_file(self,ss):
        """ """
        main_log = os.makedirs(os.path.join(self.DIRS['configuration'],'main.log'))
        now = datetime.datetime.now()
        ofh = open(main_log,'w')
        ofh.write("Analysis started: {now} .\n".format(now=now))
        ofh.close()
        pass

    def make_dir_structure(self):
        """ Write main directory scaffold structure. """
        head,tail = os.path.split(args.data)
        self.analysis_dir = os.path.join(self.args.analysis, tail, self.ANALYSIS)
        if os.path.exists(self.analysis_dir) == True:
            sys.stderr.write("Main analysis folder [\"{dir}\"] already exists.\n".format(self.analysis_dir))
            sys.stderr.write("Continue ovewriting ...\n")
        else:
            for k,v in self.SCHEMA.items():
                dir = os.path.join(self.analysis_dir,v)
                self.DIRS[k] = dir
                print dir
                os.makedirs(dir)

    def make_pre_alignment(self,):
        """ Reads trimming and filtering steps. """
        cmd = ['trim_reads.py', '-i', self.args.data  ,'-o', self.DIRS['unaligned'], '-l', 'nextera_hiseq', '-t', 'trim', 'ubam', '-n', '8', '-m', str(self.milen), '-j', self.args.json]
        print cmd
        subprocess.call(cmd,stderr=sys.stdout)

    def make_alignment(self,):
        """ Create ubam file. """
        os.makedirs(os.path.join(self.DIRS['aligned'],'raw'))
        cmd =['align_reads.py', '--ubam','-i',os.path.join(self.DIRS['unaligned'],'ubam'), '-o', os.path.join(self.DIRS['aligned'],'raw'),'-r', self.args.run_name, '-a', self.aligner, '-t', self.chem, '-j', self.args.json]
        subprocess.check_call(cmd)
        if self.cnv_mode == True:
            cmd =['align_reads.py', '--cnv_mode', '--ubam', '-i',os.path.join(self.DIRS['unaligned'],'ubam'), '-o', os.path.join(self.DIRS['cnv'],'raw'),'-r', self.args.run_name, '-a', self.aligner, '-t', self.chem, '-j', self.args.json]
            subprocess.check_call(cmd)

    def make_post_alignment(self,):
        """ """
        #Create post. Perform deduplication and realiganment around indels.
        os.makedirs(os.path.join(self.DIRS['aligned'],'post'))
        cmd =['post_alignment.py','--dedup', '--realn', '--no-recab', '-i', os.path.join(self.DIRS['aligned'],'raw'), '-o', os.path.join(self.DIRS['aligned'],'post'), '-j', self.args.json]
        subprocess.check_call(cmd)
        if self.cnv_mode == True:
            cmd =['post_alignment.py','--dedup', '--realn', '--no-recab', '-i', os.path.join(self.DIRS['cnv'],'raw'), '-o', os.path.join(self.DIRS['cnv'],'post'), '-j', self.args.json]
            subprocess.check_call(cmd)
        os.makedirs(os.path.join(self.DIRS['aligned'],'merged'))
        cmd =['merge_bams.py','-i',os.path.join(self.DIRS['aligned'], 'post'), '-o', os.path.join(self.DIRS['aligned'],'merged'), '-j', self.args.json, '-n', '8']
        subprocess.check_call(cmd)
        if self.cnv_mode == True:
            cmd =['merge_bams.py','-i',os.path.join(self.DIRS['cnv'], 'post'), '-o', os.path.join(self.DIRS['cnv'],'merged'), '-j', self.args.json, '-n', '8']
            subprocess.check_call(cmd)
            for f in os.path.join(self.DIRS['cnv'],'merged'):
                shutil.copy(os.path.join(self.DIRS['cnv'],'merged',f),os.path.join(self.args.analysis,'CNV','data'))
    
    def make_variant_calling(self,):
        """ """
        os.makedirs(os.path.join(self.DIRS['variants'],'raw'))
        cmd =['variant_calling.py','--germinal', '-i',os.path.join(self.DIRS['aligned'],'merged'), '-o', os.path.join(self.DIRS['variants'],'raw'), '-t', self.variant_caller, '-l', self.interval_list, '-j', self.args.json]
        subprocess.check_call(cmd)
        if self.cnv_mode == True:
            cmd = ['conifer_wrap.py', '-d', os.path.join(self.args.analysis,'CNV','data'), '-b', self.probes , '-o', os.path.join(self.args.analysis,'CNV','analysis')]
            subprocess.check_call(cmd)
        
    def make_cnv_calling(self,):
        """ Would need bam file id. Temporary disabled.
        Use together with mrFAST or mrsFAST mapper.
        """
        os.makedirs(self.DIRS['cnvs'])
        cmd =['conifer_wrap.py', '-i', os.path.join(self.DIRS['aligned'],'merged',), '-o', self.DIRS['cnvs']]
        subprocess.check_call(cmd)

    def make_annotation(self,):
        """ """
        os.makedirs(os.path.join(self.DIRS['variants'], 'annotated', 'varscan2'))
        cmd =['variant_annotation.py','-i',os.path.join(self.DIRS['variants'],'raw',self.variant_caller), '-o', os.path.join(self.DIRS['variants'],'annotated',self.variant_caller), '-n', '8', '-j', self.args.json]
        subprocess.check_call(cmd)
    
    def make_report(self,):
        """ """
        os.makedirs(os.path.join(self.DIRS['reports'],'varscan2'))
        #cmd = ['report_exome.py','-i',os.path.join(args.DIRS['variants'],'annotated','varscan2'), '-o', os.path.join(args.DIRS['reports'],'varscan2'), '-j', self.args.json, '-n', '8']
        cmd =['exome_reporting.py', '-i', os.path.join(args.DIRS['variants'],'annotated', self.variant_caller), '-o', args.DIRS['reports'], '-r', self.run_name]
        subprocess.check_call(cmd)
        #Coverage dir ...
        cmd = ['coverage_dir.py', '-i', os.path.join(self.DIRS['aligned'],'merged'), '-o', args.DIRS['reports'], '-r', self.run_name, '-s', '\".\"', '-t', str(self.cov_thr), '-b', self.full_bed ]
        subprocess.check_call(cmd)
        
    def submit_to_variantDB(self,):
        """ Submit variant to postgresql database. """
        cmd =['submit_to_database.py', '-i', os.path.join(args.DIRS['variants'],'annotated', self.variant_caller), '-r', self.run_name, '-a', self.analysis_name ]
        subprocess.check_call(cmd)
        
    def run_pipeline(self,):
        """ """
        self.make_dir_structure()
        self.make_pre_alignment()
        self.make_alignment()
        self.make_post_alignment()
        self.make_variant_calling()
        self.make_annotation()
        self.make_report()
        #self.submit_to_variantDB
        
    def run_variants(self,):
        self.make_dir_structure()
        self.make_pre_alignment()
        self.make_alignment()
        self.make_post_alignment()
        self.make_variant_calling()

def main(args):
    print args
    #ss = SampleSheetHiSeq(args.sample_sheet)
    ep = ExomePipeline(args)
    if 'full' in args.a_type:
        ep.run_pipeline()
        if args.cnv_mode == True:
            os.makedirs(os.path.join(args.analysis),'CNV','data')
            os.makedirs(os.path.join(args.analysis),'CNV','analysis')
            cmd =['conifer_wrap.py', '-i', os.makedirs(os.path.join(args.analysis),'CNV','data'), '-o', os.makedirs(os.paht.join(args.analysis,'CNV','analysis')), '-b', args.probes]
            subprocess.check_call(cmd)

    elif 'variants' in args.a_type:
        ep.run_variants()

    else:
        sys.exit("Analysis type option not recognized ...")
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Exome analysis for Illumina HiSeq TrueSight PIPELINE main script.\nVersion 0.1 \n Developer: Javier Diez')
    parser.add_argument('-s','--sample_sheet', required=False, help='SampleSheet file.')
    parser.add_argument('-d','--data', required=False, default='', help='Raw data directory.')
    parser.add_argument('-a','--analysis', choices=('full','variants'),required=False, default='full', help='Analysis main directory.')
    parser.add_argument('-r','--run_name', required=False, help='Run name.')
    parser.add_argument('-l','--interval_list', required=False, default='Trusight_ExomeTarget_hg19.bed', help='Run name.')
    parser.add_argument('-b','--full_bed', required=False, default='trusight_exome_manifest.bed', help='Full annotated bed.')
    parser.add_argument('-p','--probes', required=False, default='trusight_exome_manifest_probes.bed', help='Probes.')
    parser.add_argument('-t','--a_type', nargs = '+',default='full', required=True, help='Analysis type: full, ...')
    parser.add_argument('-v','--variant_caller', choices=('varscan2','gatk2','samtools'), required=False, default='varscan2', help='Variant calling algorithms to be used..')
    parser.add_argument('-j','--json', required=True, help='JSON configuration file.')
    parser.add_argument('-c','--chemistry', required=False, default='nextera',help='Sequencing chemistry.')
    parser.add_argument('-m','--mapper', choices=('novoalign','stampy','bwa-mem'),required=False, default='novoalign',help='Mapper to use.')
    parser.add_argument('--cnv_mode', dest='cnv_mode', action='store_true', default=False, help = 'Report only mitochondrial data.')
    parser.add_argument('-g','--groups', nargs = '+', required=False, default=False, help='Report only genes in desired groups. Default: all.')
    parser.add_argument('--effect', nargs = '+', choices=(1,2,3,4,5),type=int,required=False, default=False, help='Report only variants with given severity score threshold. Default: all.')
    parser.add_argument('-f','--report_format', choices=('excell','pdf','html'), required=False, default=False, help='Report only variants with given severity score threshold. Default: all.')
    parser.add_argument('-n','--self.analysis_name', required=False, default=False,help='Analysis name.')
    args = parser.parse_args()
    main(args)
